'use strict';

import * as _ from 'lodash'
import * as Wreck from 'wreck'
import * as util from 'util'
import * as config from 'config'

import DB2Connection from '@cityfurniture/database'

const SkuQohClass = require(`${__dirname}/../../../../lib/soap/sku-availability-2.0`);
const PurchaseOrders = require(`${__dirname}/../../../../lib/soap/purchase-orders-for-sku`);
const CacheClient = require(`${__dirname}/../../../../lib/cache`);

/**
 * Model class por catalog module
 * @class Model
 */
class Model {
  static config: any = config.get('application')

  /**
   * Gets substitutes for a provided sku or search string
   * @async
   * @function getSkuSubtitutes
   * @param cartId customer cart id
   * @param skuNumber sku number associated with the search
   * @param storeId store number to find substitutes
   * @param searchString search term associated with the search
   * @returns {Promise<any>} an object containing a heading and a list obj objects with the information for each substitute sku
   */
  static async getSkuSubtitutes(cartId: number, skuNumber: string, storeId: string, searchString?: string): Promise<any> {
    try {
      const selectStatement = `SELECT ITEMQUANTITY FROM QS36F.CFCARTDETAIL
      WHERE SHOPPINGCARTID = ${ cartId} AND SKU = ${skuNumber}
      FETCH FIRST ROW ONLY`

      const datadb = await DB2Connection.query(selectStatement, [])
      const quantity = datadb[0].ITEMQUANTITY;
      const skuSubEndpoint = Model.config.catalog.endpoints.skuSubstitutions;
      const options = {};
      let baseUrl = `${Model.config.catalog.protocol}://${Model.config.catalog.host}${skuSubEndpoint}`.replace(':sku', skuNumber).replace(':store_id', storeId).replace(':quantity', quantity);

      if (searchString && searchString.length > 0) {
        baseUrl = baseUrl.replace(':search_string?', searchString)
      } else {
        baseUrl = baseUrl.replace('/:search_string?', '')
      }

      const { res, payload } = await Wreck.get(baseUrl, options)
      const statusCode = res.statusCode || ''

      if (statusCode !== 200) {
        const error = `Status code: ${statusCode}`;
        return Promise.reject(error);
      }

      let error = 'Error While Retrieving Products Info:';
      if (payload) {
        const body = JSON.parse(payload.toString())

        if (body && body.status) {
          if (body.status.status) {
            const results: any = {
              heading: body.data.heading,
              product: body.data.product,
            };

            return results
          }

          error += `${body.status.description}`
          return Promise.reject(error)
        }

        error += `${body}`;
        return Promise.reject(error)
      }

      return Promise.reject(error)

    } catch (error) {
      return Promise.reject('Error Occurred While Finding Substitutions')
    }
  }

  /**
   * Gets purchase orders for a provided sku
   * @async
   * @function getPurchaseOrdersForSku
   * @param sku sku number associated with the search
   * @returns {Promise<any>} an object containing the purchase orders
   */
  static async getPurchaseOrdersForSku(sku: number): Promise<any> {
    try {
      const PurchaseOrdersObj = new PurchaseOrders(sku)

      const getAsync = util.promisify(PurchaseOrdersObj.get).bind(PurchaseOrdersObj)

      const result = await getAsync()
      const res = { purchaseOrders: result }
      return res;
    }
    catch (error) {
      return Promise.reject(error)
    }
  }

  /**
   * Gets availability information for a list of skus provided
   * @async
   * @function getSkuAvailability
   * @param skusArrayToCheck array of skus
   * @returns {Promise<any>} an object with availability information for each sku provided
   */
  static async getSkuAvailability(skusArrayToCheck: Array<string>) {
    try {
      // get SKU details first
      const prodDetails = await Model.getProductDetails(skusArrayToCheck)
      const proResult = prodDetails.itemInfo;
      const skuArray = prodDetails.idsArr;
      const skuArrayIntegers = _.map(skuArray, (oneSku) => { return _.toInteger(oneSku); });

      const SkuQoh = new SkuQohClass(skuArrayIntegers)
      const getAsync = util.promisify(SkuQoh.get).bind(SkuQoh)
      const result = await getAsync()

      for (const sku in result) {
        if (typeof proResult[sku] !== 'undefined' && proResult[sku].sku === sku) {
          result[sku].name = proResult[sku].name;
        }

        if (result[sku].availability.availabilityColor === 'R' ||
          result[sku].availability.availabilityColor === 'Y') {
          const avail = result[sku].availability.dates || {};

          if (result[sku].availability.nextAvailableDate === '0') {
            for (const oneDate in avail) {
              if (avail[oneDate].quantity > 0) {
                result[sku].availability.nextAvailableDate = avail[oneDate].date;
                break;
              }
            }
          }

          if (result[sku].availability.nextAvailableDate === '0') {
            result[sku].availability.nextAvailableDate = 'Not Available';
          }
        }
      }

      return result
    }
    catch (error) {
      return Promise.reject(error)
    }
  };

  /**
   * Gets product information for all exploded/invidivual skus
   * @async
   * @function getProductDetailsRecursively
   * @param ids array of product ids (could be skus or lp codes)
   * @returns {Promise<any>} product details
   */
  static async getProductDetailsRecursively(ids: Array<string>): Promise<any> {
    try {
      const productDetailsEndpoint = Model.config.catalog.endpoints.productDetails;
      const baseUrl = `${Model.config.catalog.protocol}://${Model.config.catalog.host}${productDetailsEndpoint}`;
      const options = {
        payload: { ids, recursive: true },
      };

      const { res, payload } = await Wreck.post(baseUrl, options)
      const statusCode = res.statusCode || ''

      if (statusCode !== 200) {
        const error = `Status code: ${statusCode}`
        return Promise.reject(error)
      }

      let error = 'Error While Retrieving Products Info:'

      if (payload) {
        const body = JSON.parse(payload.toString())

        if (body && body.status) {
          if (body.status.status) {
            const results = {
              updatedItemDetails: body.data.processedItems,
              updatedItemMissed: body.data.missingItems,
              parentSkusData: body.data.parentSkusData,
              requiredOrOptional: body.data.requiredOrOptional,
            }
            return results
          }
          error += `${body.status.description}`
          return Promise.reject(error)
        }
        error += `${body}`
        return Promise.reject(error)
      }
      return Promise.reject(error)
    }
    catch (error) {
      return Promise.reject(error)
    }
  }

  /**
   * Gets product information for all skus provided
   * @async
   * @function getProductDetails
   * @param idsArray array of product ids (could be skus or lp codes)
   * @returns {Promise<any>} product details
   */
  // we will need to get images here
  static async getProductDetails(idsArray: Array<string>): Promise<any> {
    try {
      const productDetailsEndpoint = Model.config.catalog.endpoints.productDetails
      const baseUrl = `${Model.config.catalog.protocol}://${Model.config.catalog.host}${productDetailsEndpoint}`
      const options = {
        payload: { ids: idsArray },
      }

      const { res, payload } = await Wreck.post(baseUrl, options)
      const statusCode = res.statusCode || ''

      if (statusCode !== 200) {
        const error = `Status code: ${statusCode}`
        return Promise.reject(error)
      }

      let error = 'Error While Retrieving Products Info: '
      if (payload) {
        const body = JSON.parse(payload.toString());

        if (body && body.status) {
          if (body.status.status) {
            const { data: { itemInfo, idsArr } } = body

            const results = {
              itemInfo,
              idsArr,
            }
            return results
          }
          error += `${body.status.description}`
          return Promise.reject(error)
        }
        error += `${body}`
        return Promise.reject(error)
      }
      return Promise.reject(error)
    }
    catch (error) {
      return Promise.reject(error)
    }
  }
  /**
   * Finds skusets recursively.
   * @async
   * @function getSkuSetsRecursively
   * @param skus array of skus
   * @returns {Promise<any>} product details of highest parent sku and any individual skus found
   */
  static async getSkuSetsRecursively(skus: Array<string>): Promise<any> {
    try {
      const findSkusetEndpoint = Model.config.catalog.endpoints.findSkuset
      const baseUrl = `${Model.config.catalog.protocol}://${Model.config.catalog.host}${findSkusetEndpoint}`
      const options = {
        payload: { ids: skus },
      }

      const { res, payload } = await Wreck.post(baseUrl, options)
      const statusCode = res.statusCode || ''

      if (statusCode !== 200) {
        const error = `Status code: ${statusCode}`
        return Promise.reject(error)
      }

      let error = 'Error While Retrieving Products Info:'

      if (payload) {
        const body = JSON.parse(payload.toString())

        if (body && body.status) {
          if (body.status.status) {
            const results = body.data
            return results
          }
          error += `${body.status.description}`
          return Promise.reject(error)
        }
        error += `${body}`
        return Promise.reject(error)
      }
      return Promise.reject(error)
    }
    catch (error) {
      return Promise.reject(error)
    }
  }
}

export default Model

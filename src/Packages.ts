'use strict';

import * as _ from 'lodash'
import * as Mysql from 'mysql'
import * as util from 'util'

import { default as Model } from './model'
import { IMySQLConnection, IExceptionGroup } from './interfaces'

const Helper = require(`${__dirname}/../../../../lib/helper`)

const exceptionGroups: Array<IExceptionGroup> = [
  {
    class: 19,
    subclass: 2,
    subgroup: 'ALL',
  },
  {
    class: 19,
    subclass: 3,
    subgroup: 'ALL',
  },
];

/**
 * @class Packages
 */
class Packages {
  /**
   * Finds skusets recursively.
   * @function dbTestConnection
   * @returns {Promise<any>} a connection object
   */
  static dbTestConnection(): Promise<any> {
    try {
      const options: IMySQLConnection = {
        host: '10.6.0.88',
        user: 'root',
        password: 'City001002Furniture',
        database: 'city_asap',
      }

      const connection: any = Mysql.createConnection(options);

      return connection
    }
    catch (error) {
      Promise.reject(error)
    }
  };

  /**
  * Sends arbitrary queries to MySQL
  * @async
  * @function query
  * @param query MySQL query
  * @returns {Promise<Array<any>>} results of query
  */
  static async query(query): Promise<Array<any>> {
    try {
      const conn: any = await Packages.dbTestConnection()

      if (conn) {
        const connectAsync = util.promisify(conn.connect)
        const connSuccessful = await connectAsync()

        if (connSuccessful) {
          const queryAsync = util.promisify(conn.query)
          const results: Array<any> = await queryAsync()
          conn.end()
          return results
        }
      }

      return Promise.reject('No DB Connection');
    }
    catch (error) {
      return Promise.reject(error)
    }
  };

  /**
  * Check if cart products are valid packages
  * @async
  * @function validate
  * @param skusDataFromCatalog array of documents from elasticsearch
  * @param cart array of cart objects
  * @param isFinanceDiscountLongTerm is finance didscount being applied
  * @returns {Promise<any>}
  */
  static async validate(skusDataFromCatalog: any, cart: Array<any>, isFinanceDiscountLongTerm: boolean) {
    try {
      let packageCodes = [];
      let packageCodesString = [];
      let groupsToCodes = {};

      cart.map((item) => {
        // collect the package codes to look for
        if (packageCodes.indexOf(item.packageCode) === -1 && item.packageCode.length > 0) {
          packageCodes.push(item.packageCode);
          packageCodesString.push(`'${item.packageCode}'`);
        }
      });

      if (packageCodesString.length > 0) {
        const rules = await Packages.getRules(packageCodesString)
        if (Object.keys(rules).length === 0) return { cart }

        const { originalCart, parentSkus } = await Packages.applyRules(rules, cart, skusDataFromCatalog, isFinanceDiscountLongTerm)

        return { originalCart, parentSkus }
      } else {
        return { cart }
      }
    }
    catch (error) {
      return Promise.reject(error);
    }
  };

  /**
  * Finds skusets recursively.
  * @async
  * @function applyRules
  * @param rules array of rules(objects)
  * @param cart array of cart objects
  * @param skuData skus data from catalog
  * @param isFinanceDiscountLongTerm is finance didscount being applied
  * @returns {Promise<any>} packages and a cart
  */
  static async applyRules(rules: Array<any>, cart: Array<any>, skuData: any, isFinanceDiscountLongTerm: boolean): Promise<any> {
    try {
      const newCartFormatted = Packages.formatCart(cart);
      const { itemsFromPackages, parents } = await Packages.isCartWithPackages(rules, newCartFormatted, skuData)

      if (typeof itemsFromPackages !== 'undefined' && itemsFromPackages !== null && Object.keys(itemsFromPackages).length > 0) {
        cart.map((item, index) => {
          if (typeof itemsFromPackages[item.sku] !== 'undefined') {
            const skuInfo = skuData.updatedItemDetails;
            let packagePrice = Number.parseFloat(skuInfo[item.sku].price_package);
            const retailPrice = Number.parseFloat(skuInfo[item.sku].price_retail);

            cart[index].isPackage = true;
            cart[index].skuOnSale = 'N';
            cart[index].parentSku = itemsFromPackages[item.sku].parentSku;
            cart[index].packageCode = itemsFromPackages[item.sku].packageCode;
            cart[index].savings = 0;
            cart[index].priceType = 'P';
            cart[index].shortSavings = retailPrice - packagePrice;

            if (!isFinanceDiscountLongTerm) {
              if (packagePrice <= 0) {
                packagePrice = retailPrice;
              }

              cart[index].itemPrice = packagePrice;
              cart[index].itemSalePrice = packagePrice;
              cart[index].itemAdjustedPrice = packagePrice;
              cart[index].price = packagePrice * cart[index].quantity;
              cart[index].savings = retailPrice - packagePrice;
            }
          }
        })
      }

      return { cart, parents }
    }
    catch (error) {
      return Promise.reject(error)
    }
  };

  /**
  * checks if subgroup rules applies
  * @function subgroupCheck
  * @param sample group or subgroup number
  * @param subgroupRule subgroup rule
  * @returns {boolean} if subgroup rule applies
  */
  static subgroupCheck(sample: number, subgroupRule: string): boolean {
    const subgRuleSplit = subgroupRule.split('-')

    if (subgRuleSplit[0] === 'ALL') {
      // check if there is any exclusion
      if (subgRuleSplit.length > 1) {
        const exclusions = subgRuleSplit.filter((item) => {
          if (item !== 'ALL') {
            return item
          }
        })

        if (exclusions.indexOf(String(sample)) !== -1) {
          return false
        }
      }

      return true
    }

    return (sample === Number.parseInt(subgRuleSplit[0], 10))
  }

  /**
   * Check for package exceptions
   * @function checkExceptions
   * @param {string} classId class id on elasticsearch
   * @param {string} subclassId subclass id on elasticsearch
   * @returns {Array<any>} an array of exceptions
   * @memberof Packages
   */
  static checkExceptions(classId: string, subclassId: string): Array<any> {
    const exceptions = exceptionGroups

    return exceptions.filter((item) => {
      return (item.class === Number(classId) && item.subclass === Number(subclassId))
    })
  }

  /**
  * Maps children skus with their respective parent
  * @function childMapper
  * @param {*} reOp
  * @param {*} itemInfo sku info
  * @param {*} parentsData parent skus data
  * @returns {*} a map of parent: children ids
  * @memberof Packages
  */
  static childMapper(reOp, itemInfo, parentsData) {
    let mappedChildren = {}
    let reOpsSkus = Object.keys(reOp)

    reOpsSkus.map((oneReOpSku) => {
      const skuLookedUp = Number(oneReOpSku)
      if (typeof itemInfo[skuLookedUp] !== 'undefined') {
        const { parentId } = itemInfo[skuLookedUp]
        if (typeof parentId !== 'undefined' && parentId != skuLookedUp && parentId.length > 0) {
          if (typeof mappedChildren[parentId] === 'undefined') {
            mappedChildren[parentId] = { all: [], required: [], optional: [] }
          }

          if (typeof itemInfo[parentId] === 'undefined' && typeof parentsData[skuLookedUp] === 'undefined') {
            mappedChildren[parentId].all.push(skuLookedUp)
            if (reOp[skuLookedUp]) {
              mappedChildren[parentId].required.push(skuLookedUp)
            } else {
              mappedChildren[parentId].optional.push(skuLookedUp)
            }
          }
        }
      }
    })

    return mappedChildren
  }

  /**
   * Determines if a cart has package skus
   * @async
   * @function isCartWithPackages
   * @param {Array<any>} rules array of packages rules
   * @param {*} newCartFormatted a cart object
   * @param {*} skuData sku info
   * @returns {Promise<any>}
   * @memberof Packages if cart includes a package sku
   */
  static async isCartWithPackages(rules: Array<any>, newCartFormatted, skuData): Promise<any> {
    try {
      const groups = Object.keys(newCartFormatted.cartGroups);
      const skusDataList = skuData.updatedItemDetails;
      let rulesPerPackage = [];
      let packageSkus = {};

      // get parent skus
      const skuDataFromElastic = await Model.getProductDetailsRecursively(newCartFormatted.skus)
      const parents = skuDataFromElastic.parentSkusData;
      const { requiredOrOptional } = skuDataFromElastic;
      const parentsChildren = Packages.childMapper(requiredOrOptional, skuDataFromElastic.updatedItemDetails, skuDataFromElastic.parentSkusData);

      // loop through each cart group and find if the group is a valid package
      // now we check for the rules
      for (let i = 0; i < groups.length; i++) {
        const groupsSatisfied = [];
        const key = groups[i];

        // default to isPackage false to begin with
        newCartFormatted.cartGroups[key].isPackage = false;

        if (newCartFormatted.cartGroups[key].packageCode.length > 0) {
          const packageCode = newCartFormatted.cartGroups[key].packageCode;
          const parentSku = newCartFormatted.cartGroups[key].parentSku;

          // check if the coming parent sku exists and has a matching package code
          if (typeof parents[parentSku] !== 'undefined') {
            const requiredSkusQtys = {}
            const currentRequiredSkusQtys = {}
            const packageCodeFromElastic = `P${Helper.pad(parents[parentSku].class_id)}-`
              + `${Helper.pad(parents[parentSku].subclass_id)}-${Helper.pad(parents[parentSku].subgroup_id)}`
            const skusWithPackagePricing = []

            // grab children mapping from elastic from that sku
            if (packageCodeFromElastic === packageCode) {
              const childrenRequired = parentsChildren[parentSku].required
              const childrenOptional = parentsChildren[parentSku].optional

              childrenRequired.map((oneRequiredSku) => { requiredSkusQtys[oneRequiredSku] = skuDataFromElastic.updatedItemDetails[oneRequiredSku].quantity })

              const childrenSkusFromCart = newCartFormatted.cartGroups[key].items.map((item) => {
                // we need to add the optionals that are not coming from  aspecific sku but are subs
                if (childrenOptional.indexOf(item.sku) === -1 && childrenRequired.indexOf(item.sku) === -1) {
                  childrenOptional.push(item.sku)
                }

                if (childrenRequired.indexOf(item.sku) !== -1) {
                  currentRequiredSkusQtys[item.sku] = Number(item.quantity) // Filling how much we have in the cart
                }

                return item.sku
              })

              for (const sku in requiredSkusQtys) {
                if (Object.prototype.hasOwnProperty.call(requiredSkusQtys, sku)) {
                  if (typeof skuDataFromElastic.updatedItemDetails[sku].parentId !== undefined
                    && skuDataFromElastic.updatedItemDetails[sku].parentId != sku) {
                    const parentSku = skuDataFromElastic.updatedItemDetails[sku].parentId
                    parents[parentSku].parentQty = Math.floor(currentRequiredSkusQtys[sku] / requiredSkusQtys[sku])
                  }
                }
              }

              // if required skus matched the amount of skus per group then skip, if not then continue
              if (childrenRequired.length < childrenSkusFromCart.length) {
                const allRequiredSkusExists = childrenRequired.filter(element => {
                  return childrenSkusFromCart.indexOf(element) > -1 && currentRequiredSkusQtys[element] >= requiredSkusQtys[element]
                }).length === childrenRequired.length

                // all required skus from that package already exists
                if (allRequiredSkusExists && typeof rules[packageCode] !== 'undefined') {
                  rulesPerPackage = Object.keys(rules[packageCode].groups)

                  // bring required skus into pricing for packages
                  newCartFormatted.cartGroups[key].items.map((oneCartItem, index) => {
                    if (childrenRequired.indexOf(oneCartItem.sku) !== -1) {
                      skusWithPackagePricing.push(oneCartItem)
                    }

                    // check if there is exception skus (foundation BULL!!)
                    const skuInfo = skusDataList[oneCartItem.sku]
                    const isException = Packages.checkExceptions(skuInfo.class_id, skuInfo.subclass_id)

                    if (isException.length >= 1) {
                      skusWithPackagePricing.push(oneCartItem)
                      newCartFormatted.cartGroups[key].items[index].required = true
                    }
                  })

                  const cartItems = newCartFormatted.cartGroups[key].items
                  for (let index = 0; index < rulesPerPackage.length; index++) {
                    const optionSkuCandidates = {}
                    if (groupsSatisfied.indexOf(rulesPerPackage[index]) === -1) {
                      const ruleElement = rules[packageCode].groups[rulesPerPackage[index]]
                      const cartItemsAlreadyChecked = []

                      // if there is more than one option
                      for (let optionIndex = 0; optionIndex < ruleElement.length; optionIndex++) {
                        const ruleOption = ruleElement[optionIndex]
                        optionSkuCandidates[ruleOption.id] = []


                        if (!ruleOption.hasComponents) {
                          for (let cartIndex = 0; cartIndex < cartItems.length; cartIndex++) {
                            const cartItem = cartItems[cartIndex]
                            if (childrenOptional.indexOf(cartItem.sku) !== -1 && cartItemsAlreadyChecked.indexOf(cartItem.sku) === -1) {
                              const skuInfo = skusDataList[cartItem.sku]
                              const classId = Number.parseInt(skuInfo.class_id, 10)
                              const subclassId = Number.parseInt(skuInfo.subclass_id, 10)
                              const subgroupId = Number.parseInt(skuInfo.subgroup_id, 10)
                              let matching = false

                              if (classId === ruleOption.classId && subclassId === ruleOption.subclassId) {
                                matching = true
                                // rule option is met, this rule should be marked as satisfied
                                // but we need to check for quantity first
                                if (!Packages.subgroupCheck(subgroupId, ruleOption.subgroupId)) matching = false

                                if (matching) {
                                  ruleOption.minQtyMet += Number(cartItem.quantity)

                                  optionSkuCandidates[ruleOption.id].push(cartItem)
                                  cartItemsAlreadyChecked.push(cartItem.sku)
                                  if (ruleOption.minQtyMet >= ruleOption.minQty) {
                                    optionSkuCandidates[ruleOption.id].map((item) => {
                                      skusWithPackagePricing.push(item)
                                    })
                                    optionSkuCandidates[ruleOption.id] = []
                                    groupsSatisfied.push(rulesPerPackage[index])
                                  }
                                }
                              }
                            }
                          }
                        } else {
                          let allComponentsMatch = 0
                          const componentsCount = ruleOption.components.length
                          for (let compIndex = 0; compIndex < componentsCount; compIndex++) {
                            const component = ruleOption.components[compIndex]
                            for (let cartIndex = 0; cartIndex < cartItems.length; cartIndex++) {
                              const cartItem = cartItems[cartIndex]
                              if (childrenOptional.indexOf(cartItem.sku) !== -1 && cartItemsAlreadyChecked.indexOf(cartItem.sku) === -1) {
                                const skuInfo = skusDataList[cartItem.sku]
                                const classId = Number.parseInt(skuInfo.class_id, 10)
                                const subclassId = Number.parseInt(skuInfo.subclass_id, 10)
                                const subgroupId = Number.parseInt(skuInfo.subgroup_id, 10)
                                let matching = false

                                if (classId === component.classId && subclassId === component.subclassId) {
                                  matching = true
                                  // rule option is met, this rule should be marked as satisfied
                                  // but we need to check for quantity first
                                  if (!Packages.subgroupCheck(subgroupId, component.subgroupId)) matching = false

                                  if (matching) {
                                    ruleOption.components[compIndex].minQtyMet += Number(cartItem.quantity)
                                    //ruleOption.minQtyMet += Number(cartItem.quantity)

                                    optionSkuCandidates[ruleOption.id].push(cartItem)
                                    cartItemsAlreadyChecked.push(cartItem.sku)
                                    if (ruleOption.components[compIndex].minQtyMet
                                      >= ruleOption.components[compIndex].minQty) {
                                      allComponentsMatch++
                                      if (allComponentsMatch === componentsCount) {
                                        optionSkuCandidates[ruleOption.id].map((item) => {
                                          skusWithPackagePricing.push(item)
                                        })
                                        optionSkuCandidates[ruleOption.id] = []
                                        groupsSatisfied.push(rulesPerPackage[index])
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }

                    if (rulesPerPackage.length === groupsSatisfied.length) {
                      skusWithPackagePricing.map((item) => {
                        item.parentSku = parentSku
                        packageSkus[item.sku] = item
                      })
                    }
                  }
                }
              }
            }
          }
        }
      }

      return { packageSkus, parents }

    }
    catch (error) {
      return Promise.reject(error)
    }
  };

  /**
   * Massage cart
   * @async
   * @function formatCart
   * @param {Array<any>} cart an array of cart products
   * @returns {*} a massaged cart
   * @memberof Packages
   */
  static formatCart(cart: Array<any>) {
    let newFormat: any = {
      cartGroups: {},
    }
    let parentSkusPerPackage = {}
    let sortedParents = {}
    let skusToGetFromElastic = []

    cart.map((cartItem) => {
      if (typeof newFormat.cartGroups[cartItem.groupNumber] === 'undefined') {
        newFormat.cartGroups[cartItem.groupNumber] = {
          groupNumber: 0,
          parentSku: '',
          packageCode: '',
          items: [],
        }

        sortedParents[cartItem.groupNumber] = {}
      }

      // add package code
      if (cartItem.packageCode.length > 0 && newFormat.cartGroups[cartItem.groupNumber].packageCode.length === 0) {
        newFormat.cartGroups[cartItem.groupNumber].packageCode = cartItem.packageCode
      }

      // add group number
      newFormat.cartGroups[cartItem.groupNumber].groupNumber = cartItem.groupNumber

      // try to find a parent sku
      // && newFormat.cartGroups[cartItem.groupNumber].parentSku.length === 0
      if (cartItem.parentSku.length > 0) {
        newFormat.cartGroups[cartItem.groupNumber].parentSku = cartItem.parentSku

        if (typeof sortedParents[cartItem.groupNumber][cartItem.parentSku] === 'undefined') {
          sortedParents[cartItem.groupNumber][cartItem.parentSku] = 0
        }

        sortedParents[cartItem.groupNumber][cartItem.parentSku] += 1

        if (skusToGetFromElastic.indexOf(cartItem.parentSku) === -1 && cartItem.parentSku != 0) skusToGetFromElastic.push(cartItem.parentSku)
      }

      // add all cart item fields
      newFormat.cartGroups[cartItem.groupNumber].items.push(cartItem)
    })

    Object.keys(sortedParents).map((groupParents) => {
      let topParent = ''
      let priority = 0
      Object.keys(sortedParents[groupParents]).map((parent) => {
        const oneParent = sortedParents[groupParents][parent]
        if (oneParent > priority) {
          priority = oneParent
          topParent = parent
        }
      })

      newFormat.cartGroups[groupParents].parentSku = topParent
    })

    newFormat.skus = skusToGetFromElastic
    return newFormat
  }

  /**
  * Get Rules for package codes
  * @async
  * @function getRules
  * @param {Array<string>} packageCodes package codes
  * @returns {Promise<any>}
  * @memberof Packages
  */
  static async getRules(packageCodes: Array<string>): Promise<any> {
    try {
      const rulesTable = 'subtitution_rules';
      const groupsTable = 'subtitutions_groups';
      const optionsTable = 'groups_options';
      const optionsComponents = 'options_components';
      const components = 'components';

      const query = `SELECT
        rules.code as code,
        groups.groupId as groupId,
        groups.subtitutionId as subtitutionId,
        options.minQty as minQty,
        0 as minQtyMet,
        options.id as optionId,
        options.classId as classId,
        options.subclassId as subclassId,
        options.subgroupId as subgroupId,
        options.hasComponents as hasComponents,
        components.minQty as minQtyComp,
        components.id as optionIdComp,
        components.classId as classIdComp,
        components.subclassId as subclassIdComp,
        components.subgroupId as subgroupIdComp
        FROM ${rulesTable} as rules
        LEFT JOIN ${groupsTable} as groups
        ON rules.id=groups.subtitutionId
        LEFT JOIN ${optionsTable} as options
        ON groups.groupId=options.groupId
        LEFT JOIN ${optionsComponents} as optionsComponents
        ON options.id=optionsComponents.optionId AND options.hasComponents=1
        LEFT JOIN ${components} as components
        ON optionsComponents.componentId=components.id
        WHERE rules.code IN (${packageCodes.join(',')})`

      const dbResult = await Packages.query(query)
      const rules = Packages.massageRules(dbResult)

      return rules
    }
    catch (error) {
      return Promise.reject(error)
    }
  }

  /**
   * Massage packafge Rules
   * @async
   * @function massageRules
   * @param {Array<any>} dbResult db data
   * @returns info for each rule
   * @memberof Packages
   */
  static massageRules(dbResult: Array<any>) {
    const rules = {}

    dbResult.map((oneRule) => {
      if (typeof rules[oneRule.code] === 'undefined') rules[oneRule.code] = { groups: {} }
      if (typeof rules[oneRule.code].groups[oneRule.groupId] === 'undefined') {
        rules[oneRule.code].groups[oneRule.groupId] = []
      }

      let optionExists = false
      rules[oneRule.code].groups[oneRule.groupId].map((option, index) => {
        if (option.id === oneRule.optionId) {
          optionExists = true
        }
      })

      if (!optionExists) {
        const newOption = {
          id: oneRule.optionId,
          code: oneRule.code,
          minQty: oneRule.minQty,
          minQtyMet: oneRule.minQtyMet,
          classId: oneRule.classId,
          subclassId: oneRule.subclassId,
          subgroupId: oneRule.subgroupId,
          hasComponents: (oneRule.hasComponents === 1),
        }

        rules[oneRule.code].groups[oneRule.groupId].push(newOption)
      }

      if (oneRule.hasComponents === 1) {
        // dig again
        rules[oneRule.code].groups[oneRule.groupId].map((option, index) => {
          if (typeof option.components === 'undefined') {
            rules[oneRule.code].groups[oneRule.groupId][index].components = []
          }

          const newComponent = {
            id: oneRule.optionId,
            code: oneRule.code,
            minQty: oneRule.minQtyComp,
            minQtyMet: 0,
            classId: oneRule.classIdComp,
            subclassId: oneRule.subclassIdComp,
            subgroupId: oneRule.subgroupIdComp,
          }

          rules[oneRule.code].groups[oneRule.groupId][index].components.push(newComponent)
        })
      }
    })

    return rules
  }
}

export default Packages


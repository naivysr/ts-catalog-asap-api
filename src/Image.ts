'use strict';
/**
 * @class Image
 */
class Image {

  /**
   * @static
   * @param {Array<any>} images array of image objects
   * @param {number} sku sku number
   * @returns {Array<any>} array of parsed images
   * @memberof Image
   */
  static multiple(images: Array<any>, sku: number): Array<any> {
    let front = {}
    let imageFound: boolean = false

    for (let i = 0; i < images.length; i++) {
      if (images[i].url.indexOf(`${sku}N00`) !== -1) {
        front = images[i]
        images.splice(i, 1)
        images.unshift(front)
        imageFound = true
        break
      }
    }

    if (!imageFound) {
      for (let j = 0; j < images.length; j++) {
        if (images[j].url.indexOf('F00') !== -1) {
          front = images[j]
          images.splice(j, 1)
          images.unshift(front)
        }
      }
    }

    return images
  }

  /**
   * Genrate image URL
   * @static
   * @param {Array<any>} images array of images objects
   * @param {number} sku sku number
   * @returns {string} image URL
   * @memberof Image
   */
  static smallImage(images: Array<any>, sku: number): string {
    let image = 'http://dummyimage.com/500X500/' +
      'ffffff/949494.jpg&text=Image+not+available'

    if (images.length > 0) {
      for (let i = 0; i < images.length; i++) {
        if (images[i].url.indexOf(`${sku}N00`) !== -1) {
          image = images[i].url
          break
        }
      }

      if (image.indexOf('dummyimage') !== -1) {
        for (let j = 0; j < images.length; j++) {
          if (images[j].url.indexOf('F00') !== -1) {
            image = images[j].url
            break
          }

          if (image.indexOf('dummyimage') !== -1 && images[j].type === 'S') {
            image = images[j].url
          }
        }
      }

      // fallback to first image
      if (image.indexOf('dummyimage') !== -1) {
        image = images[0].url
      }

      return image
    }

    return image
  }
}

export default Image


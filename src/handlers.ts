'use strict';

import { default as payloads } from './payloads'
import { default as Model } from './model'

const Helper = require(`${__dirname}/../../../../lib/helper`)

class Handlers {
  static async getSkuAvailabilityHandler(request, reply): Promise<any> {
    try {
      const { headers } = request
      let validationError = ''
      let validationCheck

      if (Helper.isApiVersion(headers, '2.0')) {
        validationCheck = payloads.availability_v2.validate(request.payload)
      } else {
        validationCheck = payloads.availability.validate(request.payload)
      }

      // error validation
      if (validationCheck.error !== null) {
        validationError = validationCheck.error.message
        return reply(Helper.getErrorMessage(validationError)).code(200)
      }

      const skus = (request.payload.skus) ? request.payload.skus.map(item => `${item}`) : request.payload.map(item => `${item}`)
      const result = await Model.getSkuAvailability(skus)

      return reply(Helper.getSuccessMessage(result))
    }
    catch (error) {
      return reply(Helper.getErrorMessage(error)).code(200)
    }
  }

  static async getPurchaseOrdersForSkuHandler(request: any, reply: any): Promise<any> {
    try {
      const { params: { sku } } = request
      const result = await Model.getPurchaseOrdersForSku(sku)

      return reply(Helper.getSuccessMessage(result))
    }
    catch (error) {
      reply(Helper.getErrorMessage(error)).code(200)
    }
  }

  static async getSkuSubtitutes(request: any, reply: any): Promise<any> {
    try {
      const sku = request.params.sku;
      const storeId = request.params.store_id;
      const cartId = request.params.cart_id;
      let searchString = '';

      if (typeof request.params.search_string !== 'undefined') {
        searchString = request.params.search_string;
      }

      const result = await Model.getSkuSubtitutes(cartId, sku, storeId, searchString)
      return reply(Helper.getSuccessMessage(result))
    }
    catch (error) {
      return reply(Helper.getErrorMessage(error)).code(200)
    }
  }
}

export default Handlers


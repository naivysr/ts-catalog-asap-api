interface IMySQLConnection {
  host: string
  user: string,
  password: string,
  database: string,
}

interface IExceptionGroup {
  class: number,
  subclass: number,
  subgroup: string,
}


export { IMySQLConnection, IExceptionGroup }
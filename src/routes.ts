'use strict';

import * as Joi from 'joi'

// local modules
import { default as Handlers } from './handlers'
import { default as payloads } from './payloads'

const Helper = require(`${__dirname}/../../../../lib/helper`)

const Routes = [
  {
    method: 'POST',
    path: '/catalog/availability',
    config: {
      tags: ['api'],
      description: 'Get SKUs availability',
      notes: 'Based on 9 day window for stock available, incoming PO, and reservations',
      handler: Handlers.getSkuAvailabilityHandler,
    },
  },

  {
    method: 'GET',
    path: '/catalog/purchaseorders/{sku}',
    config: {
      tags: ['api'],
      description: 'Gets the open POs for the requested sku',
      notes: 'Returns the PO number, the ETA date, and the qty on order',
      validate: {
        params: {
          sku: Joi.number().integer().required(),
        },
        failAction: Helper.invalidated,
      },
      handler: Handlers.getPurchaseOrdersForSkuHandler,
    },
  },

  {
    method: 'GET',
    path: '/catalog/subtitute/{cart_id}/{sku}/{store_id}/{search_string?}',
    config: {
      tags: ['api'],
      description: 'Gets the open POs for the requested sku',
      notes: 'Returns the PO number, the ETA date, and the qty on order',
      validate: {
        params: {
          cart_id: Joi.number().integer().required(),
          sku: Joi.number().integer().required(),
          store_id: Joi.number().integer().required(),
          search_string: Joi.string().optional(),
        },
        failAction: Helper.invalidated,
      },
      handler: Handlers.getSkuSubtitutes,
    },
  },
]

export default Routes


'use strict';

import * as Model from './model'

/**
 * @class Availability
 */
class Availability {
  private skus: Array<string | number>
  private hasSkus: boolean
  /**
   *Creates an instance of Availability.
   * @param {(Array<string | number>)} skusArray array of skus
   * @memberof Availability
   */
  constructor(skusArray: Array<string | number>) {
    this.skus = [];
    this.hasSkus = !!skusArray

    if (this.hasSkus && skusArray.length > 0) {
      this.skus = skusArray
    }
  }
}

export default Availability

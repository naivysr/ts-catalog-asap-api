import * as Joi from 'joi'

const payloads = {
  availability: Joi.array().items(Joi.number().integer().required()),

  availability_v2: Joi.object().keys({
    skus: Joi.array().items(Joi.number().integer().required()),
  }),
}

export default payloads

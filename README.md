# Catalog Plugin (v1.0.7)

Catalog Plugin encapsulates the core functionality of ASAPi's catalog and serves it as a pluggable module.


## Built With

* [Node v8.9.1](https://nodejs.org/en/) - JavaScript runtime
* [Npm 5.5.1](https://www.npmjs.com/) - Ecosystem of Open Source Libraries
* [TypeScript](https://www.typescriptlang.org/) and JavaScript - Languages

### Prerequisites

You will need the items listed below in order to run and use this module:

* Node v8.9.1
* Typescript
* ASAP-API (https://bitbucket.org/cityfurniture/asap-api)

Node and Typescript
```
nvm install v8.9.1
npm install -g typescript
```

ASAPi API
```
git clone git@bitbucket.org:cityfurniture/asap-api.git
git fetch && git checkout master
```


### Getting Up and Running

*** Before proceeding, complete all steps in the Prerequisites section. ***

1.  Compile this Project

    - Get `.env` file
    - `npm install`
    - Compile Source Directory: `tsc -w`

1.  Run **ASAPi-API**

    - Get `.env` file
    - `npm install`
    - Start API Server


### Examples


## Authors

* **Naivys Tirado** - *Initial work*
* **Terrence Dacres** - *Collaborator*

## Acknowledgments

* Kudos to the entire City Furniture IT department
* Your continued diligence and perseverance manifests boundless innovation


## Missing Topics
* Running Tests
* Deployment
* Versioning
* License
